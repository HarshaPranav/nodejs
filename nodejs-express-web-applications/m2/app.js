var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var session = require('express-session');

var app = express();

/*
// For MSSQL Conenction string
var sql = require('mssql');

var config = {
    user: 'books',
    password: 'books',
    server: 'SKOTALENOVA1', // You can use 'localhost\\instance' to connect to named instance
    database: 'books',

    options: {
        encrypt: true // Use this if you're on Windows Azure
    }
};

sql.connect(config, function(err) {
    console.log(err);
});
*/

var port = process.env.PORT || 3000;

var nav =  [{
    Link: '/books',
    Text: 'Books'
},{
    Link: '/Authors',
    Text: 'Authors'
}];

var bookRouter = require('./src/routes/bookRoutes')(nav);
var adminRouter = require('./src/routes/adminRoutes')(nav);
var authRouter = require('./src/routes/authRoutes')(nav);

app.use(express.static('public')); //app.use(express.static('src/views'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(session({secret:'library'}));
require('./src/config/passport')(app);

app.set('views', './src/views');
/* For EJS*/
app.set('view engine','ejs');
/*
//For Handle Bars
var handlebars = require('express-handlebars');
app.engine('.hbs',handlebars({extname: '.hbs'}));
app.set('view engine','.hbs');
*/
/*
//For Jade Template engine
app.set('view engine','jade');
*/

app.use('/books', bookRouter);
app.use('/Admin', adminRouter);
app.use('/Auth', authRouter);

app.get('/', function (req, res) {
    res.render('index', {
                title: 'hello from app.js',
                glist:['x','y','z'],
                nav: nav
            });
});

app.listen(port, function (err) {
    console.log('Runnin server on port ' + port);
});
